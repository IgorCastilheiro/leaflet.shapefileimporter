module.exports = function (grunt) {
    var banner = '/*! Version: <%= pkg.version %>\nDate: <%= grunt.template.today("yyyy-mm-dd") %> */\n';

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            dist: {
                files: {
                    'leaflet.shapefile-importer.min.css': 'leaflet.shapefile-importer.css'
                }
            }
        },
        bump: {
            options: {
                files: ['bower.json', 'package.json'],
                commitFiles: ['bower.json', 'commit.json'],
                push: false
            }
        }
    });

    grunt.registerTask('default', ['cssmin']);
};
