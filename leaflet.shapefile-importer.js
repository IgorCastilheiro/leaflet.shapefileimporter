/**
 * Created by Luan on 17/09/2015.
 */
L.Control.ShapeFileImporter = L.Control.extend({
    options: {
        position: 'topleft',
        title: 'Importar Shape File'
    },


    initialize: function (layerManager, usSpinnerService) {
        this.layerManager = layerManager;
        this.usSpinnerService = usSpinnerService;
    },

    onAdd: function (map) {
        var mainDiv = L.DomUtil.create('div', 'leaflet-control-import-shp leaflet-bar leaflet-control');
        mainDiv.id = 'dropzone';

        // var link = L.DomUtil.create('a', 'leaflet-bar-part leaflet-bar-part-single', mainDiv);
        var link = L.DomUtil.get("shape_upload");
        link.title = "Importar ShapeFile";
        L.DomEvent.addListener(link, "click", function () {
            btnUp.click();
        });

        var btnUp = L.DomUtil.create('input', 'fileUploader', link);
        btnUp.type = 'file';
        btnUp.id = 'flpShapeFile';
        btnUp._handleFile = this.handleFile;
        btnUp._layerManager = this.layerManager;
        btnUp._usSpinnerService = this.usSpinnerService;

        btnUp.onchange = function () {
            var file = document.getElementById('flpShapeFile').files[0];
            if (file) {
                this._handleFile(file, map, this._layerManager, this._usSpinnerService);
            }
        };

        var dropBox = document.getElementById("map");
        dropBox.addEventListener("dragenter", dragenter, false);
        dropBox.addEventListener("dragover", dragover, false);
        dropBox.addEventListener("drop", drop, false);
        dropBox._handleFile = this.handleFile;
        dropBox.addEventListener("dragleave", function () {
            map.scrollWheelZoom.enable();
        }, false);

        function dragenter(e) {
            e.stopPropagation();
            e.preventDefault();
            map.scrollWheelZoom.disable();
        }

        function dragover(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        function drop(e) {
            e.stopPropagation();
            e.preventDefault();
            map.scrollWheelZoom.enable();
            var dt = e.dataTransfer;
            var files = dt.files;

            //var i = 0;
            var len = files.length;
            if (!len) {
                return
            }

            this._handleFile(files[0], map, btnUp._layerManager, btnUp._usSpinnerService);

        }

        return mainDiv;
    },

    handleFile: function (file, map, layerM, usSpinnerService) {

        usSpinnerService.spin('spinner-1');
        var geo = L.geoJson({features: []},
            {
                onEachFeature: function popUp(f, l) {
                    var out = [];
                    if (f.properties) {
                        for (var key in f.properties) {
                            out.push(key + ": " + f.properties[key]);
                        }
                        l.bindPopup(out.join("<br />"));
                    }
                }

            }
        );

        var wfunc = function (base, cb) {
            importScripts('../bower_components/leaflet-shapefile-importer/shp.min.js');
            shp(base).then(cb);
        };

        var worker = cw({
            data: wfunc
        }, 2);

        if (file.name.slice(-3) === 'zip') {
            var reader = new FileReader();
            reader.onload = function () {
                if (this.readyState !== 2 || this.error) {
                    usSpinnerService.stop('spinner-1');
                    // return;
                }
                else {
                    worker.data(this.result)
                        .then(function (data) {
                                geo.addData(data);
                                map.addLayer(geo);

                                geo.extras = {
                                    title: data.fileName,
                                    tab: "default",
                                    legend: '/styles/images/shp.png',
                                    isMarker: true
                                };
                                geo.options.visible = true;

                                if (layerM && Array.isArray(layerM)) {
                                    layerM.push(geo);
                                }
                                else if (layerM) {
                                    layerM.addOverlay(geo, data.fileName);
                                }
                                usSpinnerService.stop('spinner-1');
                            },
                            function (a) {
                                console.log(a);
                                usSpinnerService.stop('spinner-1');
                            }
                        );
                }
            };
            reader.readAsArrayBuffer(file);
        }
    }
});

L.control.shapefileimporter = function (layerManager, usSpinnerService) {
    return new L.Control.ShapeFileImporter(layerManager, usSpinnerService);
};